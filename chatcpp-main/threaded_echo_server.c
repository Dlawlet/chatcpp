
#include <netinet/in.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>  

#include "./common.h"


int nclients = 0;
pthread_mutex_t mutex ;
void *echo(void *socket_ptr) {
  printf("Client is connected\n");
  int client_socket = *((int *)socket_ptr);
  ssize_t nbytes = 1;
  while (nbytes > 0) {
    char *buffer;
    nbytes = receive(client_socket, (void **)&buffer);
    printf("Received %ld bytes: %s\n", nbytes, buffer);
    if (nbytes > 0) {
      for (int i=0;i==nclients;i++){
        pthread_mutex_lock(&mutex);
        nbytes = ssend(clients[i], buffer, nbytes);
        pthread_mutex_unlock(&mutex);
      }
      free(buffer);
    }
  }
  printf("Client disconnected\n");
  return NULL;
}

int main(int argc, char *argv[]) {
  int opt = 1;
  int socketServer= checked(socket(AF_INET, SOCK_STREAM, 0));
  checked(setsockopt(socketServer, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)));

  struct sockaddr_in addrServer;
  addrServer.sin_family = AF_INET;
  addrServer.sin_addr.s_addr = INADDR_ANY;
  addrServer.sin_port = htons(8080);
  checked(bind(socketServer, (struct sockaddr *)&addrServer, sizeof(addrServer)));
  checked(listen(socketServer, 3));
  int clients[1024];
  size_t addrServerlen = sizeof(addrServer);
  pthread_t threads[1024]; 
  pthread_mutex_init(&mutex,NULL);
  while (true) {
    clients[nclients] = checked(accept(socketServer, (struct sockaddr *)&addrServer, (socklen_t *)&addrServerlen));
    nclients++;
    if (pthread_create(&threads[nclients], NULL, echo, &clients
    ) != 0) {
      perror("Could not create the thread\n");
      exit(1);
    }
    
  }
  pthread_mutex_destroy(&mutex);
  for (int i=0;i<nclients;i++){
    close(clients[i]);
  }
  close(socketServer);
  return 0;
}
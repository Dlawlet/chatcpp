#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>  //close

#include "common.h"

int main(int argc, char *argv[]) {
  int opt = 1;
  int master_socket = checked(socket(AF_INET, SOCK_STREAM, 0));
  checked(setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)));

  // type of socket created
  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(8080);
  checked(bind(master_socket, (struct sockaddr *)&address, sizeof(address)));
  checked(listen(master_socket, 3));

  socklen_t addrlen = sizeof(address);
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(master_socket, &readfds);
  int max_fd = master_socket;
  // Le seul file descriptor ajouté est celui du socket "maître"
  select(max_fd + 1, &readfds, NULL, NULL, NULL);

  if (!FD_ISSET(master_socket, &readfds)) {
    fprintf(stderr, "Impossible d'arriver ici (en théorie)\n");
    exit(1);
  } else {
    // Vu que le master socket qui a des donnees, c'est une nouvelle connexion.
    int nouveau_sock = checked(accept(master_socket, (struct sockaddr *)&address, &addrlen));
    // ...
    // Il faut maintenant faire quelque chose d'utile avec ce nouveau socket, comme l'ajouter
    // au fd_set pour détecter lorsqu'il y a de nouvelles données à lire.
  }
  return 0;
}
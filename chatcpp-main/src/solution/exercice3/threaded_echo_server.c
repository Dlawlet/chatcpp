// Example code: A simple server side code, which echos back the received message.
// Handle multiple socket connections with select and fd_set on Linux
#include <netinet/in.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>  //close

#include "../common.h"

void *echo(void *socket_ptr) {
  printf("Client is connected\n");
  int client_socket = *((int *)socket_ptr);
  ssize_t nbytes = 1;
  while (nbytes > 0) {
    char *buffer;
    nbytes = receive(client_socket, (void **)&buffer);
    printf("Received %ld bytes: %s\n", nbytes, buffer);
    if (nbytes > 0) {
      nbytes = ssend(client_socket, buffer, nbytes);
      free(buffer);
    }
  }
  printf("Client disconnected\n");
  return NULL;
}

int main(int argc, char *argv[]) {
  int opt = 1;
  int master_socket = checked(socket(AF_INET, SOCK_STREAM, 0));
  checked(setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)));

  // type of socket created
  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(8080);
  checked(bind(master_socket, (struct sockaddr *)&address, sizeof(address)));
  checked(listen(master_socket, 3));

  size_t addrlen = sizeof(address);
  int clients[1024];
  pthread_t threads[1024];
  int nclients = 0;
  while (true) {
    clients[nclients] = checked(accept(master_socket, (struct sockaddr *)&address, (socklen_t *)&addrlen));
    if (pthread_create(&threads[nclients], NULL, echo, &clients[nclients]) != 0) {
      perror("Could not create the thread\n");
      exit(1);
    }
    nclients++;
  }

  return 0;
}
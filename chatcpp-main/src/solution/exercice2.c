#include <pthread.h>
#include <stdio.h>

void* thread_function(void* data_to_print) {
  int* data = (int*)data_to_print;
  for (int i = 0; i < 100; i++) {
    printf("%d\n", *data);
  }
}

int main(int argc, char const* argv[]) {
  pthread_t tids[2];  // thread ids
  int a = 1;
  int b = 2;
  pthread_create(&tids[0], NULL, thread_function, &a);
  pthread_create(&tids[1], NULL, thread_function, &b);
  pthread_join(tids[0], NULL);
  pthread_join(tids[1], NULL);
  printf("End of main\n");
  return 0;
}

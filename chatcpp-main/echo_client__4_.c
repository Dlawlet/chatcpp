// Client side C/C++ program to demonstrate Socket programming
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "./common.h"

int main(int argc, char const *argv[]) {
  int socketclient = checked(socket(AF_INET, SOCK_STREAM, 0));
  struct sockaddr_in addrclient;
  addrclient.sin_family = AF_INET;
  addrclient.sin_port = htons(8080);

  // Conversion de string vers IPv4 ou IPv6 en binaire
  checked(inet_pton(AF_INET, "127.0.0.1", &addrclient.sin_addr));

  checked(connect(socketclient, (struct sockaddr *)&addrclient, sizeof(addrclient)));
  char buffer[1024];
  ssize_t nbytes = 1;
  while (nbytes > 0 && fgets(buffer, 1024, stdin)) {
    // Supprimer le \n
    size_t len = strlen(buffer);
    buffer[len - 1] = '\0';
    // On garde la même taille de string pour explicitement envoyer le '\0'
    nbytes = ssend(socketclient, buffer, len);
    if (nbytes > 0) {
      char *recvbuffer;
      nbytes = receive(socketclient, (void *)&recvbuffer);
      if (nbytes > 0) {
        printf(" %s\n", recvbuffer);
        free(recvbuffer);
      }
    }
  }
  return 0;
}

FLAGS=-Wall -pedantic


all:
	gcc -pthread -o Client client.c $(FLAGS)
	gcc -o Server server.c $(FLAGS)

clean:
	rm -f Client Server

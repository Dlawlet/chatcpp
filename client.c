// Client side C/C++ program to demonstrate Socket programming
#include <arpa/inet.h>
#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "./common.h"
pthread_mutex_t mut_send;
pthread_mutex_t mut_recv;

typedef struct {
    char *usernm;
    int *sockt ;
}struct_usrnm;

void *senderfct(void *USER) {
  char buffer[1024];
  ssize_t nbytes = 1;
  struct_usrnm user = *((struct_usrnm *)USER);
  int socket = *((int *)(user.sockt));
  char *usr = user.usernm ; 
  while (nbytes > 0 && fgets(buffer, 1024, stdin)) {
    // Supprimer le \n
    pthread_mutex_lock(&mut_send);
    char *ps = malloc(40);
    strcpy(ps, usr);
    strcat(ps, " : ");
    strcat(ps, buffer);  
    size_t len = strlen(ps);
    ps[len - 1] = '\0';
    // On garde la même taille de string pour explicitement envoyer le '\0'
    nbytes = ssend(socket, len, ps);
    free(ps);
    pthread_mutex_unlock(&mut_send);
  }
  if (fgets(buffer, 1024, stdin) == NULL){
      printf(" Fermeture du client \n ");
      exit(0);
  }
  return NULL;
}

void *receiverfct(void *USER) {
  ssize_t nbytes = 1;
  struct_usrnm user = *((struct_usrnm *)USER);
  int socket = *((int *)(user.sockt));
  while (nbytes > 0 ) {
    pthread_mutex_lock(&mut_recv);
    char *recvbuffer;
    nbytes = receive(socket, (void *)&recvbuffer);
    if (nbytes > 0 ) {
      printf("---> %s\n", recvbuffer);
      free(recvbuffer);
      }
    pthread_mutex_unlock(&mut_recv);
  }
  return NULL;
}

int main(int argc, char *argv[]) {
  if (argc < 4){
    printf(" need more parameters. please send your username Ip and port");
    return 1;
  }
  else {
  int sock = checked(socket(AF_INET, SOCK_STREAM, 0));
  struct sockaddr_in serv_addr;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(atoi(argv[3]));

  // Conversion de string vers IPv4 ou IPv6 en binaire
  checked(inet_pton(AF_INET, argv[2], &serv_addr.sin_addr));

  checked(connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)));
  
  int* res = NULL;
  pthread_t sender;
  pthread_t receiver;
  struct_usrnm USER;
  USER.sockt = &sock;
  USER.usernm = argv[1];
  pthread_mutex_init(&mut_send,NULL);
  pthread_mutex_init(&mut_recv,NULL);
  pthread_create(&sender, NULL, senderfct, &USER);
  pthread_create(&receiver, NULL, receiverfct, &USER);
  pthread_join ( sender ,NULL);
  pthread_join ( receiver ,NULL);
  pthread_mutex_destroy(&mut_send);
  pthread_mutex_destroy(&mut_recv);
  if (*res==2){
    close(sock);
  }
  return 0;
  }
}
